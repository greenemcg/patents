package com.mcg.patents.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
@Slf4j
public class InvalidStatusException extends RuntimeException {
    public InvalidStatusException(String message) {
        super(message);
        log.error(message);
    }
}