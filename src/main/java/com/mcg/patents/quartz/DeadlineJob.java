package com.mcg.patents.quartz;

import com.mcg.patents.services.DeadlineReportService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DeadlineJob implements Job {
    @Autowired
    private DeadlineReportService jobService; // fails if use Constructor injection

    public void execute(JobExecutionContext context) {

        log.info("Job ** {} ** fired @ {}", context.getJobDetail().getKey().getName(), context.getFireTime());

        jobService.executeJob();

        log.info("Next job scheduled @ {}", context.getNextFireTime());
    }
}
