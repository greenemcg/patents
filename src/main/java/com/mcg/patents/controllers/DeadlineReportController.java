package com.mcg.patents.controllers;

import com.mcg.patents.services.DeadlineReportService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/deadline-report")
@SuppressWarnings("unused")
public class DeadlineReportController {
    private final DeadlineReportService deadlineReportService;

    public DeadlineReportController(DeadlineReportService deadlineReportService) {
        this.deadlineReportService = deadlineReportService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<String> getLastReport() {
        return new ResponseEntity<>(deadlineReportService.getLastReport(), HttpStatus.OK);
    }
}

