package com.mcg.patents.controllers;

import com.mcg.patents.dto.PatentDto;
import com.mcg.patents.services.PatentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/patents")
@SuppressWarnings("unused")
public class PatentController {
    private final PatentService patentService;

    public PatentController(PatentService patentService) {
        this.patentService = patentService;
    }

    @PostMapping
    ResponseEntity<PatentDto> create(@Valid @RequestBody PatentDto patent) {
        return new ResponseEntity<>(patentService.create(patent), HttpStatus.OK);
    }

    @PutMapping
    ResponseEntity<PatentDto> update(@Valid @RequestBody PatentDto patent) {
        return new ResponseEntity<>(patentService.update(patent), HttpStatus.OK);
    }

    @DeleteMapping(path = "/{uuid}")
    ResponseEntity delete(@PathVariable String uuid) {
        patentService.delete(uuid);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping
    ResponseEntity<List<PatentDto>> getAll() {
        return new ResponseEntity<>(patentService.getAll(), HttpStatus.OK);
    }
}

