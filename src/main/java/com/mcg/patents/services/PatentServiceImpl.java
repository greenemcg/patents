package com.mcg.patents.services;

import com.mcg.patents.dto.PatentDto;
import com.mcg.patents.exceptions.ResourceNotFoundException;
import com.mcg.patents.model.Patent;
import com.mcg.patents.repositories.PatentRepository;
import com.mcg.patents.services.components.PatentConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class PatentServiceImpl implements PatentService {
    private final PatentRepository patentRepository;
    private final PatentConverter patentConverter;

    public PatentServiceImpl(PatentRepository patentRepository, PatentConverter patentConverter) {
        this.patentRepository = patentRepository;
        this.patentConverter = patentConverter;

    }

    @Override
    @Transactional
    public PatentDto create(PatentDto patentDto) {
        Patent patent = patentConverter.convertToEntity(patentDto, new Patent());
        log.debug("Creating new patent: {}", patent);
        return persistAndConvert(patent);
    }

    @Override
    @Transactional
    public PatentDto update(PatentDto patentDto) {
        Patent patent = patentConverter.convertToEntity(patentDto, findPatent(patentDto.getUuid()));
        log.debug("Updating patent: {}", patent);
        return persistAndConvert(patent);
    }


    @Override
    @Transactional
    public void delete(String uuid) {
        Patent patent = findPatent(uuid);
        log.info("Deleting patent with uuid: {}", uuid);

        patentRepository.delete(patent);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PatentDto> getAll() {
        List<PatentDto> patentListToReturn = new ArrayList<>();

        patentRepository.findAll()
                .forEach(p -> patentListToReturn.add(patentConverter.convertToDto(p)));

        log.debug("Found {} patents", patentListToReturn.size());

        return patentListToReturn;
    }

    private PatentDto persistAndConvert(Patent patent) {
        return patentConverter.convertToDto(patentRepository.save(patent));
    }

    private Patent findPatent(String uuid) {
        return patentRepository.findByUuid(uuid)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Cannot find patent with uuid: %s.", uuid)));
    }
}
