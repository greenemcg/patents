package com.mcg.patents.services;

import com.mcg.patents.dto.PatentDto;

import java.util.List;

public interface PatentService {
    PatentDto create(PatentDto patentDto);

    List<PatentDto> getAll();

    void delete(String uuid);

    PatentDto update(PatentDto patent);

}
