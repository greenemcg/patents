package com.mcg.patents.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.mcg.patents.dto.PatentDto;
import com.mcg.patents.exceptions.ResourceNotFoundException;
import com.mcg.patents.model.DeadlineReport;
import com.mcg.patents.model.Patent;
import com.mcg.patents.repositories.DeadlineReportRepository;
import com.mcg.patents.repositories.PatentRepository;
import com.mcg.patents.services.components.PatentConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DeadlineReportServiceImpl implements DeadlineReportService {
    private static final int DEADLINE_HOURS = 12;

    private final PatentConverter patentConverter;
    private final PatentRepository patentRepository;
    private final DeadlineReportRepository deadlineReportRepository;


    public DeadlineReportServiceImpl(PatentConverter patentConverter,
                                     PatentRepository patentRepository,
                                     DeadlineReportRepository deadlineReportRepository) {
        this.patentConverter = patentConverter;
        this.patentRepository = patentRepository;
        this.deadlineReportRepository = deadlineReportRepository;
    }

    @Override
    @Transactional
    public void executeJob() {
        log.info("The deadline report job has begun...");

        String json = createJson();

        DeadlineReport deadlineReport = new DeadlineReport();
        deadlineReport.setJson(json);
        deadlineReportRepository.save(deadlineReport);

        log.info("Report job has finished with deadlineReport id {}... ", deadlineReport.getId());
    }

    @Override
    public String getLastReport() {
        Optional<DeadlineReport> optionalDeadlineReport = deadlineReportRepository.findFirstByOrderByCreatedDesc();

        if (optionalDeadlineReport.isPresent()) {
            return optionalDeadlineReport.get().getJson();
        } else {
            throw new ResourceNotFoundException("Cannot find any deadline reports");
        }
    }

    private String createJson() {
        List<Patent> patentsInDeadlineState =
                patentRepository.findDeadlines(LocalDateTime.now().minusHours(DEADLINE_HOURS));
        return createJson(patentsInDeadlineState);
    }

    private String createJson(List<Patent> patentsInDeadlineState) {
        List<PatentDto> dtos = createDtos(patentsInDeadlineState);

        try {
            ObjectMapper mapper = new ObjectMapper()
                    .registerModule(new ParameterNamesModule())
                    .registerModule(new Jdk8Module())
                    .registerModule(new JavaTimeModule())
                    .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(dtos);
        } catch (JsonProcessingException e) {
            log.error("Cannot parse json, {}", e);
            return "{}";
        }
    }

    private List<PatentDto> createDtos(List<Patent> patentsInDeadlineState) {
        return patentsInDeadlineState.stream()
                .map(patentConverter::convertToDto)
                .collect(Collectors.toList());
    }
}