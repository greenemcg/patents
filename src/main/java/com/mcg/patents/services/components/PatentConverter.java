package com.mcg.patents.services.components;

import com.mcg.patents.dto.PatentDto;
import com.mcg.patents.model.Patent;
import com.mcg.patents.model.PatentStatus;
import org.springframework.stereotype.Component;

@Component
public class PatentConverter {
    private final AuthorComponent authorComponent;

    public PatentConverter(AuthorComponent authorComponent) {
        this.authorComponent = authorComponent;
    }

    public PatentDto convertToDto(Patent patent) {
        PatentDto patentDto = new PatentDto();
        patentDto.setUuid(patent.getUuid());
        patentDto.setStatus(patent.getStatus().toString());
        patentDto.setUrl(patent.getUrl());
        patentDto.setDeadline(patent.getDeadline());
        patentDto.setCreated(patent.getCreated());
        patentDto.setAuthors(authorComponent.convert(patent.getAuthors()));
        return patentDto;
    }

    public Patent convertToEntity(PatentDto patentDto, Patent patent) {
        patent.getAuthors().clear();

        patent.setDeadline(patentDto.getDeadline());
        patent.setUrl(patentDto.getUrl());
        patent.setStatus(PatentStatus.find(patentDto.getStatus()));
        patent.setAuthors(authorComponent.createOrFind(patentDto.getAuthors()));

        return patent;
    }
}
