package com.mcg.patents.services.components;

import com.mcg.patents.model.Author;
import com.mcg.patents.repositories.AuthorRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Slf4j
public class AuthorComponent {
    private final AuthorRepository authorRepository;

    public AuthorComponent(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

     Set<Author> createOrFind(List<String> authors) {
        return authors.stream()
                .map(this::findOrCreateAuthor)
                .collect(Collectors.toSet());
    }

    List<String> convert(Set<Author> authors) {
        return authors.stream()
                .map(Author::getName)
                .collect(Collectors.toList());
    }

    private Author findOrCreateAuthor(String name) {
        Optional<Author> optionalAuthor = authorRepository.findByNameIgnoreCase(name);

        return optionalAuthor.orElse(createAuthor(name));
    }

    private Author createAuthor(String name) {
        log.debug("Creating new author: {}", name);
        Author author = new Author();
        author.setName(name);
        return author;
    }
}
