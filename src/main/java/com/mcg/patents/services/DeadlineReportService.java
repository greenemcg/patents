package com.mcg.patents.services;

public interface DeadlineReportService {
    void executeJob();

    String getLastReport();
}