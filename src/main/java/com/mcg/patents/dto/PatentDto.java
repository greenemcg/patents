package com.mcg.patents.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class PatentDto {
    private String uuid;

    @NotNull
    @Size(min = 10, max = 500)
    private String url;

    @NotNull
    private String status;

    @NotNull
    private LocalDateTime deadline;

    private LocalDateTime created;

    @NotEmpty(message = "At least one author is required")
    @Valid
    private List<String> authors = new ArrayList<>();
}
