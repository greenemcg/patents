package com.mcg.patents.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.PrePersist;
import java.time.LocalDateTime;

@Entity(name = "DeadlineReport")
@Data
@NoArgsConstructor
public class DeadlineReport {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private LocalDateTime created;

    @Column(nullable = false)
    @Lob
    private String json;

    @PrePersist
    private void prePersist() {
        created = LocalDateTime.now();
    }
}
