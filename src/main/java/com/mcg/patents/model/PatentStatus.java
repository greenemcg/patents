package com.mcg.patents.model;

import com.mcg.patents.exceptions.InvalidStatusException;

import java.util.Arrays;

public enum PatentStatus {
    PENDING, APPROVED, REJECTED;

    public static PatentStatus find(String statusString) {
        return Arrays.stream(PatentStatus.values())
                .filter(statusEnum -> statusEnum.toString().equalsIgnoreCase(statusString))
                .findFirst()
                .orElseThrow(() -> new InvalidStatusException(String.format("Unsupported status type: %s.", statusString)));
    }
}
