package com.mcg.patents.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity(name = "Patent")
@Data
@NoArgsConstructor
public class Patent {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, unique = true)
    private String uuid;

    @Column(nullable = false, length = 500)
    private String url;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private PatentStatus status;

    @Column(nullable = false)
    private LocalDateTime deadline;

    @Column(nullable = false)
    private LocalDateTime created;

    @NotNull
    @ManyToMany(cascade = CascadeType.PERSIST)
    private Set<Author> authors = new HashSet<>();

    @PrePersist
    private void prePersist() {
        uuid = UUID.randomUUID().toString();
        created = LocalDateTime.now();
    }
}
