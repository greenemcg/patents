package com.mcg.patents.repositories;

import com.mcg.patents.model.Author;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AuthorRepository extends CrudRepository<Author, Long> {
    @Query("select a from Author a where lower(a.name) = lower(?1)")
    Optional<Author> findByNameIgnoreCase(String name);
}
