package com.mcg.patents.repositories;

import com.mcg.patents.model.Patent;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface PatentRepository extends CrudRepository<Patent, Long> {
    Optional<Patent> findByUuid(String uuid);

    @Query("select p from Patent p where p.status = 'PENDING' and p.deadline <= ?1")
    List<Patent> findDeadlines(LocalDateTime creationDateTim);
}
