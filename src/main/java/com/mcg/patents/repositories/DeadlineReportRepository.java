package com.mcg.patents.repositories;

import com.mcg.patents.model.DeadlineReport;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface DeadlineReportRepository extends CrudRepository<DeadlineReport, Long> {

    Optional<DeadlineReport> findFirstByOrderByCreatedDesc();


}
