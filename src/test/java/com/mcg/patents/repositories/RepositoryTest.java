package com.mcg.patents.repositories;

import com.mcg.patents.model.Author;
import com.mcg.patents.model.Patent;
import com.mcg.patents.model.PatentStatus;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RepositoryTest {
    private static final String AUTHOR_NAME = "Thomas Edison";
    private static final LocalDateTime DEADLINE_CUT_OFF = LocalDateTime.now().minusHours(12);

    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private AuthorRepository authorRepository;
    @Autowired
    private PatentRepository patentRepository;

    @After
    public void tearDown() {
        patentRepository.findAll().forEach(entityManager::remove);
        authorRepository.findAll().forEach(entityManager::remove);
    }

    @Test
    public void testPatentFindDeadlines_FindNone() {
        assertTrue(patentRepository.findDeadlines(DEADLINE_CUT_OFF).isEmpty());
        createPatent();
        assertTrue(patentRepository.findDeadlines(DEADLINE_CUT_OFF).isEmpty());
    }

    @Test
    public void testPatentFindDeadlines_FindOne() {
        assertTrue(patentRepository.findDeadlines(DEADLINE_CUT_OFF).isEmpty());
        Patent patent = createPatentWithDeadline(LocalDateTime.now().minusHours(13));
        assertFalse(patentRepository.findDeadlines(DEADLINE_CUT_OFF).isEmpty());

        patent.setStatus(PatentStatus.REJECTED);
        entityManager.persist(patent);
        assertTrue(patentRepository.findDeadlines(DEADLINE_CUT_OFF).isEmpty());

        patent.setStatus(PatentStatus.APPROVED);
        entityManager.persist(patent);
        assertTrue(patentRepository.findDeadlines(DEADLINE_CUT_OFF).isEmpty());


        //lets set the conditions back so we can find it again as a deadline
        patent.setStatus(PatentStatus.PENDING);
        patent.setDeadline(LocalDateTime.now().minusHours(113));
        entityManager.persist(patent);
        assertFalse(patentRepository.findDeadlines(DEADLINE_CUT_OFF).isEmpty());
    }

    /**
     * Verify the case insensitive query for finding authors by name
     */
    @SuppressWarnings("OptionalGetWithoutIsPresent")
    @Test
    public void testAuthorFindByNameIgnoreCase() {
        findOrCreateAuthor();

        verifyAuthor();

        Optional<Author> optionalAuthorUpCased = authorRepository.findByNameIgnoreCase(AUTHOR_NAME.toUpperCase());
        assertEquals(AUTHOR_NAME, optionalAuthorUpCased.get().getName());

        Optional<Author> optionalAuthorNotFound = authorRepository.findByNameIgnoreCase(AUTHOR_NAME.toUpperCase() + "S");
        assertFalse(optionalAuthorNotFound.isPresent());
    }

    @Test
    public void testPatentFindByUuid() {
        Patent patent = createPatent();

        assertNotNull(patent.getCreated());
        assertEquals(PatentStatus.PENDING, patent.getStatus());

        verifyAuthor(); //ensure author persisted

        assertTrue(patentRepository.findByUuid(patent.getUuid()).isPresent());
        assertFalse(patentRepository.findByUuid(patent.getUuid() + "S").isPresent());
    }

    private void verifyAuthor() {
        assertTrue(authorRepository.findByNameIgnoreCase(AUTHOR_NAME).isPresent());
    }

    private Author findOrCreateAuthor() {
        Optional<Author> optionalAuthor = authorRepository.findByNameIgnoreCase(AUTHOR_NAME);

        return optionalAuthor.orElse(createAndPersistAuthor());
    }

    private Author createAndPersistAuthor() {
        Author author = new Author();
        author.setName(AUTHOR_NAME);
        return entityManager.persist(author);
    }

    private Patent createPatentWithDeadline(LocalDateTime deadline) {
        Patent patent = createPatent();
        patent.setDeadline(deadline);

        return entityManager.persist(patent);
    }

    private Patent createPatent() {
        Patent patent = new Patent();
        patent.setUrl("http://patentsrus.com/1234/gizmo.pdf");
        patent.getAuthors().add(findOrCreateAuthor());
        patent.setDeadline(LocalDateTime.now().plusDays(100));
        patent.setStatus(PatentStatus.PENDING);
        return entityManager.persist(patent);
    }
}