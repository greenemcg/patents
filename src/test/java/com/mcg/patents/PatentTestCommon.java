package com.mcg.patents;

import com.mcg.patents.dto.PatentDto;
import com.mcg.patents.model.Author;
import com.mcg.patents.model.Patent;
import com.mcg.patents.model.PatentStatus;

import java.time.LocalDateTime;
import java.util.Collections;

public interface PatentTestCommon {
    String UUID = "c4ea4551-cd67-41bb-9718-6a292e2f3684";
    String AUTHOR = "Alan Turing";

    default PatentDto createPatentDto() {
        PatentDto patentDto = new PatentDto();
        patentDto.setUuid(UUID);
        patentDto.setStatus(PatentStatus.PENDING.toString());
        patentDto.setAuthors(Collections.singletonList("Alan Turing"));
        return patentDto;
    }

    default Patent createPatent() {
        Patent patent = new Patent();
        patent.setUuid(UUID);
        patent.setStatus(PatentStatus.PENDING);
        patent.setUrl("http://lawyers.com/telsa/1234.pdf");
        patent.setDeadline(LocalDateTime.now().plusDays(100));
        patent.setCreated(LocalDateTime.now());
        patent.getAuthors().add(createAuthor());

        return patent;
    }

    default Author createAuthor() {
        Author author = new Author();
        author.setName(AUTHOR);
        return author;
    }
}
