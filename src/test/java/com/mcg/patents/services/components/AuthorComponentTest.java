package com.mcg.patents.services.components;

import com.mcg.patents.model.Author;
import com.mcg.patents.repositories.AuthorRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthorComponentTest {
    private static final String AUTHOR_1 = "Nikola Tesla";
    private static final String AUTHOR_2 = "Steve Wozniak";

    @Mock
    private AuthorRepository authorRepository;
    @InjectMocks
    private AuthorComponent authorComponent;

    @Test
    public void createOrFind() {
        final Long defaultId = 1L;

        when(authorRepository.findByNameIgnoreCase(AUTHOR_1)).thenReturn(Optional.empty());
        when(authorRepository.findByNameIgnoreCase(AUTHOR_2)).thenReturn(Optional.of(createAuthor(AUTHOR_2, defaultId)));

        List<String> authorList = Arrays.asList(AUTHOR_1, AUTHOR_2);
        Set<Author> authors = authorComponent.createOrFind(authorList);

        authors.forEach(author -> {
            if (author.getName().equals(AUTHOR_1)) {
                assertNull(author.getId());
            } else if (author.getName().equals(AUTHOR_2)) {
                assertEquals(defaultId, author.getId());
            } else {
                fail("Found unknown author: " + author.getName());
            }
        });

        verify(authorRepository, times(authorList.size())).findByNameIgnoreCase(anyString());
    }

    @Test
    public void convertAuthors() {
        List<Author> authorsList = Arrays.asList(createAuthor(AUTHOR_1, 1L), createAuthor(AUTHOR_2, 2L));

        Set<Author> authorsSet = new HashSet<>(authorsList);

        List<String> stringList = authorComponent.convert(authorsSet);

        assertEquals(authorsSet.size(), stringList.size());
        assertTrue(stringList.contains(AUTHOR_1));
        assertTrue(stringList.contains(AUTHOR_2));

        verifyZeroInteractions(authorRepository);
    }

    private Author createAuthor(String name, Long id) {
        Author author = new Author();
        author.setId(id);
        author.setName(name);
        return author;
    }
}