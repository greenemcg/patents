package com.mcg.patents.services.components;

import com.mcg.patents.PatentTestCommon;
import com.mcg.patents.dto.PatentDto;
import com.mcg.patents.exceptions.InvalidStatusException;
import com.mcg.patents.model.Patent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PatentConverterTest implements PatentTestCommon {
    @Mock
    private AuthorComponent authorComponent;
    @InjectMocks
    private PatentConverter patentConverter;

    @Test
    public void convertToDto() {
        Patent patent = createPatent();
        when(authorComponent.convert(patent.getAuthors())).thenReturn(Collections.singletonList(AUTHOR));

        PatentDto patentDto = patentConverter.convertToDto(patent);

        verifyDto(patent, patentDto);
        assertEquals(patent.getUuid(), patentDto.getUuid());

        verify(authorComponent, times(1)).convert(patent.getAuthors());
    }

    @Test
    public void convertToEntity_Sucess() {
        PatentDto patentDto = createPatentDto();

        when(authorComponent.createOrFind(patentDto.getAuthors()))
                .thenReturn(new HashSet<>(Collections.singletonList(createAuthor())));

        Patent patent = patentConverter.convertToEntity(patentDto, new Patent());

        verifyDto(patent, patentDto);

        verify(authorComponent, times(1)).createOrFind(patentDto.getAuthors());
    }


    @Test(expected = InvalidStatusException.class)
    public void convertToEntity_BadStatus() {
        PatentDto patentDto = createPatentDto();
        patentDto.setStatus("OOPS");

        patentConverter.convertToEntity(patentDto, new Patent());
    }


    private void verifyDto(Patent patent, PatentDto patentDto) {
        assertEquals(patent.getUrl(), patentDto.getUrl());
        assertEquals(patent.getStatus().toString(), patentDto.getStatus());
        assertEquals(patent.getDeadline(), patentDto.getDeadline());
        assertEquals(patent.getCreated(), patentDto.getCreated());
        assertEquals(patent.getAuthors().size(), patentDto.getAuthors().size());
        assertEquals(1, patentDto.getAuthors().size());
        assertEquals(patent.getAuthors().iterator().next().getName(), patentDto.getAuthors().get(0));
    }
}