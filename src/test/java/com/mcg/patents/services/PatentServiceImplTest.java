package com.mcg.patents.services;

import com.mcg.patents.PatentTestCommon;
import com.mcg.patents.dto.PatentDto;
import com.mcg.patents.exceptions.ResourceNotFoundException;
import com.mcg.patents.model.Patent;
import com.mcg.patents.repositories.PatentRepository;
import com.mcg.patents.services.components.PatentConverter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PatentServiceImplTest implements PatentTestCommon {
    @Mock
    private PatentRepository patentRepository;
    @Mock
    private PatentConverter patentConverter;
    @InjectMocks
    private PatentServiceImpl patentService;

    @Test
    public void create_Success() {
        Patent patent = createPatent();
        PatentDto patentDto = createPatentDto();

        when(patentRepository.save(patent)).thenReturn(patent);
        when(patentConverter.convertToEntity(any(), any())).thenReturn(patent);
        when(patentConverter.convertToDto(patent)).thenReturn(patentDto);

        PatentDto patentCreated = patentService.create(createPatentDto());

        assertEquals(patentDto, patentCreated);

        verify(patentRepository, times(1)).save(patent);
        verify(patentConverter, times(1)).convertToEntity(any(), any());
        verify(patentConverter, times(1)).convertToDto(patent);
        verify(patentRepository, never()).findByUuid(anyString());
        verify(patentRepository, never()).delete(any());
    }

    @Test
    public void update_Success() {
        PatentDto patentDto = createPatentDto();
        Patent patent = createPatent();

        when(patentConverter.convertToEntity(patentDto, patent)).thenReturn(patent);
        when(patentConverter.convertToDto(patent)).thenReturn(patentDto);
        when(patentRepository.findByUuid(UUID)).thenReturn(Optional.of(patent));
        when(patentRepository.save(patent)).thenReturn(patent);


        PatentDto patentUpdated = patentService.update(patentDto);
        assertEquals(patentDto, patentUpdated);

        verify(patentRepository, times(1)).findByUuid(UUID);
        verify(patentRepository, times(1)).save(patent);
        verify(patentConverter, times(1)).convertToEntity(patentDto, patent);
        verify(patentConverter, times(1)).convertToDto(patent);
        verify(patentRepository, never()).delete(any());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void update_FailNotFound() {
        PatentDto requestDto = createPatentDto();

        when(patentRepository.findByUuid(UUID)).thenReturn(Optional.empty());

        patentService.update(requestDto);
    }

    @Test
    public void delete_Success() {
        Patent patent = createPatent();

        when(patentRepository.findByUuid(UUID)).thenReturn(Optional.of(patent));

        patentService.delete(UUID);

        verify(patentRepository, times(1)).delete(patent);
        verify(patentRepository, times(1)).findByUuid(UUID);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void delete_FailNotFound() {
        when(patentRepository.findByUuid(UUID)).thenReturn(Optional.empty());

        patentService.delete(UUID);
    }

    @Test
    public void getAll() {
        List<Patent> patentList = Arrays.asList(createPatent(), createPatent());

        when(patentRepository.findAll()).thenReturn(patentList);

        List<PatentDto> patentDtos = patentService.getAll();
        assertEquals(patentList.size(), patentDtos.size());
    }
}