# Patent Service

Application Uses port 9999

To build run all test 

`mvn clean install`

To run the application
 
`mvn spring-boot:run`

System uses default maven `spring-boot-maven-plugin`


## Patents Endpoint

mapping `/patents`

**POST** 

to create send POST request with json RequestBody:

'{
 "url": "http://my.repo.com",
  "status": "PENDING",
    "deadline": "2019-03-04T02:10:46.21"
  "authors": ["Mike Greene3","Mike Greene5"]
}`

Will return:

`{
    "uuid": "779db06f-b491-476d-9a7f-19d9a1bebc7e",
    "url": "http://my.repo.com",
    "status": "PENDING",
    "deadline": "2019-03-04T02:10:46.21",
    "created": "2019-03-05T11:10:46.249",
    "authors": [
        "Mike Greene5",
        "Mike Greene3"
    ]
}

**GET**

 Will return a list of all patents in the DB.
 
***PUT*** 

To update send a record similar to POST but be sure to include the uuid.

the created date cannot be changed;


**DELETE**   

delete the uuid must be appended as a path variable

E.G. `/patents/779db06f-b491-476d-9a7f-19d9a1bebc7e`


**Notes**

javax.validation is performed on the input records see PatentDto class for details.

If a record is not found by the uuid in the POST and DELETE methods a 404 error is issued.

If a invalid status is sent a 421 error is sent.

Valid statuses case insensitive:
`PENDING, APPROVED, REJECTED`


## Deadline Report Endpoint 

mapping` /deadline-report`

**GET**

 Will return the last report in the DB, or 404 if no reports are found.
 
 The report is a list of patent DTOs.

-----

**Developer Notes**

The system will leave orphan authors when patents are deleted.

No Unit tests done on the controller, would just be mock city.

No Unit tests on the quartz job and the deadline report ran out of time.

System uses a h2 in memory DB to use the web console go here no authentication required.

`http://localhost:9999/h2-console/`

